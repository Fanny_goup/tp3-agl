import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import poste.ColisExpress;
import poste.ColisExpressInvalide;
import poste.Recommandation;

public class AppTestColisExpress {
	@Test
	public void testColisExpressDePlusDe30kg(){
		assertThrows(ColisExpressInvalide.class, ()-> new ColisExpress("ftt", "oui", "non", 31, 15,Recommandation.deux, "drnu,", 0, false), "Colis Express plus lourd que 30KG");
	}
}
